import QtQuick 2.6
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.13 as Kirigami

import org.kde.splinelogo 1.0

Kirigami.ScrollablePage {
    title: i18n("Host Settings")

    actions: [
        Kirigami.Action {
            text: i18n("Save")
            onTriggered: {
                Config.host = hostField.text
                Config.port = portField.value
                pageStack.layers.pop()
            }
        }
    ]

    Kirigami.FormLayout {
        Controls.TextField {
            id: hostField
            Kirigami.FormData.label: i18n("Host Name")
            text: Config.host
        }
        Controls.SpinBox {
            id: portField
            Kirigami.FormData.label: i18n("Port")
            value: Config.port
            from: 1
            to: 65535
        }
    }
}
