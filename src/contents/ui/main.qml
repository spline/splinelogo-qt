/*
    SPDX-License-Identifier: GPL-2.0-or-later
    SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
*/

import QtQuick 2.6
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.13 as Kirigami

import QtQuick.Dialogs

import org.kde.splinelogo 1.0

Kirigami.ApplicationWindow {
    id: root

    title: i18n("Spline Logo")

    Component.onCompleted: {
        LogoCtrl.host = Qt.binding(() => Config.host)
        LogoCtrl.port = Qt.binding(() => Config.port)
    }

    minimumWidth: Kirigami.Units.gridUnit * 20
    minimumHeight: Kirigami.Units.gridUnit * 20

    pageStack.initialPage: Kirigami.ScrollablePage {
        title: i18n("Change Color")
        actions: [
            Kirigami.Action {
                text: i18n("Configure")
                icon.name: "settings-configure"
                onTriggered: pageStack.layers.push("HostSettings.qml")
            },
            Kirigami.Action {
                text: i18n("Set custom color")
                icon.name: "color-management"
                onTriggered: {
                    colorDialog.open()
                }
            }
        ]

        ListView {
            model: ListModel {
                ListElement {
                    name: "Rainbow Colors"
                    colorId: "rb"
                }
                ListElement {
                    name: "Rainbow Cycle"
                    colorId: "rbc"
                }
                ListElement {
                    name: "Color Wipe Red"
                    colorId: "cwr"
                }
                ListElement {
                    name: "Color Wipe Green"
                    colorId: "cwg"
                }
                ListElement {
                    name: "Color Wipe Blue"
                    colorId: "cwb"
                }
                ListElement {
                    name: "Theater Rainbow Cycle"
                    colorId: "trc"
                }
                ListElement {
                    name: "Theater Chase Red"
                    colorId: "tcr"
                }
                ListElement {
                    name: "Theater Chase Green"
                    colorId: "tcg"
                }
                ListElement {
                    name: "Theater Chase Blue"
                    colorId: "tcb"
                }
            }
            delegate: Controls.ItemDelegate {
                required property string name
                required property string colorId
                required property int index

                text: name
                onClicked: LogoCtrl.preset = colorId

                width: parent.width
                height: 40
            }


            Controls.BusyIndicator {
                anchors.centerIn: parent
                visible: LogoCtrl.loading
            }

            ColorDialog {
                id: colorDialog
                title: "Please choose a color"
                selectedColor: "#FFFFFF"
                onAccepted: {
                    LogoCtrl.color = colorDialog.selectedColor
                }
            }
        }
    }
}
