/*
    SPDX-License-Identifier: GPL-2.0-or-later
    SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
*/

#include <KLocalizedContext>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QUrl>
#include <QtQml>

#include "logoctrl.h"
#include "config.h"

using namespace Qt::Literals;

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName(u"KDE"_s);
    QCoreApplication::setOrganizationDomain(u"kde.org"_s);
    QCoreApplication::setApplicationName(u"splinelogo"_s);

    QQmlApplicationEngine engine;

    qmlRegisterSingletonType<LogoCtrl>("org.kde.splinelogo", 1, 0, "LogoCtrl", [](QQmlEngine *engine, QJSEngine *) {
        return new LogoCtrl(engine);
    });
    qmlRegisterSingletonInstance<Config>("org.kde.splinelogo", 1, 0, "Config", Config::self());
    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    int ret = app.exec();
    Config::self()->save();
    return ret;
}
