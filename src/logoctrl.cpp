#include "logoctrl.h"

LogoCtrl::LogoCtrl(QObject *parent)
    : QObject(parent)
    , m_socket(this)
{
    connect(&m_socket, &QTcpSocket::connected, this, &LogoCtrl::loadingChanged);
}

void LogoCtrl::setColor(QColor color)
{
    if (sendColor(colorCommand(color))) {
        m_currentColor = std::move(color);
        Q_EMIT colorChanged();
    }
}

void LogoCtrl::setPreset(const QString &preset)
{
    if (sendColor(preset)) {
        m_preset = preset;
        Q_EMIT presetChanged();
    }
}

bool LogoCtrl::loading() const {
    switch (m_socket.state()) {
    case QAbstractSocket::ConnectingState:
    case QAbstractSocket::HostLookupState:
        return true;
    default:
        return false;
    }
}

bool LogoCtrl::sendColor(const QString &string)
{
    if (m_context) {
        qDebug() << "Already setting a color";
        return false;
    }

    if (m_host.isEmpty() || m_port == 0) {
        qDebug() << "Invlid host or port" << m_host << m_port;
        return false;
    }

    qDebug() << "opening connection to" << m_host << m_port;
    m_context = std::make_unique<QObject>();
    connect(&m_socket, &QTcpSocket::stateChanged, m_context.get(), [this, string]() {
        qDebug() << "state changed" << m_socket.state();
        switch (m_socket.state()) {
        case QTcpSocket::ConnectedState: {
            const auto cmd = string.toUtf8();
            m_socket.write(cmd.data());
            qDebug() << "disconnecting";
            m_socket.disconnectFromHost();
            break;
        }
        case QTcpSocket::UnconnectedState:
            m_context->deleteLater();
            m_context.reset(nullptr);
        default:
            break;
        }
    });
    connect(&m_socket, &QTcpSocket::errorOccurred, this, [=, this] {
        m_context->deleteLater();
        m_context.reset(nullptr);
        qDebug() << "error" << m_socket.errorString();
        return false;
    });
    m_socket.connectToHost(m_host, m_port, QTcpSocket::ReadWrite);

    return true;
}

QString LogoCtrl::colorCommand(const QColor &c)
{
    return QStringLiteral("%1 %2 %3").arg(c.red()).arg(c.green()).arg(c.blue());
}
