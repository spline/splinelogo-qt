﻿#pragma once

#include <QObject>
#include <QTcpSocket>
#include <QColor>
#include <QHostAddress>
#include <QDebug>

#include <memory>

class LogoCtrl : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(QString preset READ preset WRITE setPreset NOTIFY presetChanged)
    Q_PROPERTY(bool loading READ loading NOTIFY loadingChanged)
    Q_PROPERTY(QString host READ host WRITE setHost NOTIFY hostChanged)
    Q_PROPERTY(quint16 port READ port WRITE setPort NOTIFY portChanged)

public:
    explicit LogoCtrl(QObject *parent = nullptr);

    [[nodiscard]] QColor color() const {
        return m_currentColor;
    }

    void setColor(QColor color);

    Q_SIGNAL void colorChanged();

    [[nodiscard]] QString preset() const {
        return m_preset;
    }

    void setPreset(const QString &preset);

    Q_SIGNAL void presetChanged();

    [[nodiscard]] bool loading() const;

    Q_SIGNAL void loadingChanged();

    [[nodiscard]] QString host() const {
        return m_host;
    }

    void setHost(const QString &host) {
        m_host = host;
        Q_EMIT hostChanged();
    }

    Q_SIGNAL void hostChanged();

    [[nodiscard]] quint16 port() const {
        return m_port;
    }

    void setPort(quint16 port) {
        m_port = port;
        Q_EMIT portChanged();
    }

    Q_SIGNAL void portChanged();

private:
    bool sendColor(const QString &string);
    QString colorCommand(const QColor &c);

    std::unique_ptr<QObject> m_context = nullptr;
    QColor m_currentColor;
    QString m_preset;
    QTcpSocket m_socket;
    QString m_host;
    quint16 m_port = 0;
};

